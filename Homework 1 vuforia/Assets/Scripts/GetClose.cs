using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetClose : MonoBehaviour
{
    public GameObject cactus1;
    public GameObject cactus2;
    string guiText = "";

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance (cactus1.transform.position, cactus2.transform.position);
        float walid = distance / 10;
        guiText = walid.ToString();
        if (distance < 0.1)
        {
            cactus1.GetComponent<Animator>().SetBool("isFighting", true);
            cactus2.GetComponent<Animator>().SetBool("isFighting", true);
        }
        else
        {
            cactus1.GetComponent<Animator>().SetBool("isFighting", false);
            cactus2.GetComponent<Animator>().SetBool("isFighting", false);
        }
    }
    
    void OnGUI()
    {
        GUIStyle localStyle = new GUIStyle();
        localStyle.normal.textColor = Color.red;
        localStyle.fontSize = 70;
        GUI.Label(new Rect(20, 50, Screen.width - 20, 30), guiText+" CM ", localStyle);
    }
}
