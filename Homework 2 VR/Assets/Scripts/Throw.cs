using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using CommonUsages = UnityEngine.XR.CommonUsages;

public class Throw : MonoBehaviour
{
    public GameObject toThrow;
    public GameObject parent;
    public GameObject wall;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.G))
        {
            toThrow.GetComponent<Rigidbody>().AddForce(parent.transform.forward * 600);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "destination")
        {
            var wallRenderer = wall.GetComponent<Renderer>();
            wallRenderer.material.SetColor("_Color", Color.green);
        }
    }
}
