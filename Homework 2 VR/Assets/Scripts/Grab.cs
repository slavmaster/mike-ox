using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Grab : MonoBehaviour
{
    public GameObject hand;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            hand.GetComponent<Animator>().SetBool("isGrabbing", true);
        }
    }
}
